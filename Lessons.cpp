﻿#include <iostream>
#include <string>
#include <cmath>
#define DEBUG

using namespace std;

class Vector
{
public:
	Vector()
	{
		x = 0;
		y = 0;
		z = 0;
	}

	Vector(float x, float y, float z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	friend Vector operator+(const Vector& a, const Vector& b);
	friend Vector operator-(const Vector& a, const Vector& b);
	friend ostream& operator<<(ostream& out, const Vector& v);
	friend Vector operator*(const Vector& a, const int b);
	friend istream& operator>>(istream& in, Vector& v);
	friend bool operator>(const Vector& a, const Vector& b);

	operator float()
	{
		return sqrt(x * x + y * y + z * z);
	}

	float operator[](int index)
	{
		switch (index)
		{
		case 0: return x; break;
		case 1: return y; break;
		case 2: return z; break;
		default: cout << "Index error"; return 0; break;
		}
	}

private:
	float x;
	float y;
	float z;
};

Vector operator+(const Vector& a, const Vector& b)
{
	return Vector(a.x + b.x, a.y + b.y, a.z + b.z);
}

Vector operator-(const Vector& a, const Vector& b)
{
	return Vector(a.x - b.x, a.y - b.y, a.z - b.z);
}

ostream& operator<<(ostream& out, const Vector& v)
{
	out << ' ' << v.x << ' ' << v.y << ' ' << v.z;
	return out;
}

istream& operator>>(istream& in, Vector& v)
{ 
	in >> v.x;
	in >> v.y;
	in >> v.z;

	return in;
}

Vector operator*(const Vector& a, const int b)
{ return Vector(b * a.x, b * a.y, b * a.z); }

bool operator>(const Vector& a, const Vector& b)
{
	return false;
}

int main()
{
	Vector v1;
	Vector v2(3, 4, 5);
	Vector v3;

	cin >> v1;

	v3 = v1 + v2;
	cout << v1 + v2 << endl;
	cout << v1 - v2 << endl;
	cout << v1 * 4;
	cout << v2[1] << endl;
}
