﻿#include <iostream>
#include <string>
#include <cmath>
#include <vector>
#define DEBUG

using namespace std;

class Fraction
{
public:
	Fraction(int numerator, int denominator)
	{
		if (denominator == 0) throw runtime_error("re");
		if (numerator > 15 || denominator > 15 || numerator < 0 || denominator < 0) throw exception("e");
		cout << "Numerator = " << numerator << endl;
		cout << "Denominator = " << denominator << endl;
	}

private:

};

int main()
{
	int num, den;
	cout << "Enter positive numbers less than 15" << endl;
	cout << "Enter numerator: ";
	cin >> num;
	cout << "Enter denominator: ";
	cin >> den;

	try
	{
		Fraction(num, den);
	}
	catch (const runtime_error& re)
	{
		cout << "Error: denominator cannot be equal to zero" << endl;
	}
	catch (const exception& e)
	{
		cout << "Error: Invalid numbers";
	}
}